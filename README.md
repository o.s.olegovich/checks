# Запуск проекта

### 1. Установить все инфраструктурные вещи необходимые для сервиса.
- 1.1 Загрузить образы docker - docker-compose pull
- 1.2 Устновить зависимости проекта - pip3 install -r req.txt

### 2. Для запуска проекта необходимо запустить docker-compose
- 2.1 cd docker
- 2.2 docker-compose up
- 2.3 cd ../

### 3. Cделать миграции, создать пользвателя, загрузить fixtures

- 3.1 /manage.py migrate
- 3.2 /manage.py createsuperuser
- 3.3 /manage.py loaddata

### 4. Запуск проекта и воркера
- 4.1 Запуск воркера /manage.py rqworker default
- 4.2 Запуск проекта /manage.py runserver
