import json
import requests

from base64 import b64encode

url = 'http://localhost:8080/'
data = {
    'contents': b64encode(open('convert.html').read().encode("utf-8")).decode('ascii'),
}
headers = {
    'Content-Type': 'application/json',    # This is important
}
response = requests.post(url, data=json.dumps(data), headers=headers)

# Save the response contents to a file
with open('file.pdf', 'wb') as f:
    f.write(response.content) 
