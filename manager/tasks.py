from django_rq import enqueue, job
from .models import Check
from django.template.loader import render_to_string
import json
import requests
import os

from base64 import b64encode


def html_to_pdf(content):
    """
    Функция переводит контент в base64 и отправляем на микросервис htmltopdf, возвращает pdf-data
    """
    url = 'http://localhost:8081/'
    data = {
        'contents': b64encode(content.encode("utf-8")).decode('ascii'),
    }
    headers = {
        'Content-Type': 'application/json',
    }

    response = requests.post(url, data=json.dumps(data), headers=headers)
    return response.content


def __generate_pdf__(check_id):
    """
    Синхронная функция для генерации pdf по check_id
    """
    # Проверям есть ли такой чек (может его удалили, а задачу нет)
    if Check.objects.filter(id=check_id).exists() is False:
        return

    # Получем в чек
    check = Check.objects.get(id=check_id)
    context = check.order
    html_content = ''

    # выберам шаблон по типу чека, кладем его в шаблонезатор и рендарим
    if check.type == Check.KITCHEN:
        html_content = render_to_string('kitchen_check.html', context)
    elif check.type == Check.CLIENT:
        html_content = render_to_string('client_check.html', context)
    if html_content != '':
        pdf_data = html_to_pdf(html_content)
        try:
            # Сохраняем его в нужный каталог и даем правильное имя
            if os.path.exists('media/pdf/') is False:
                os.makedirs('media/pdf/')
            filename = 'media/pdf/' + str(check.order['id']) + '_' + check.type + '.pdf'
            with open(filename, 'wb') as f:
                f.write(pdf_data)
            # Указываем путь до файла в записе, так же меняем стаус на RENDERED
            check.pdf_file.name = filename
            check.status = Check.RENDERED
            check.save()
        except OSError:
            print("OSError in manager/tasks.py")


def generate_pdf(check_id):
    """
     Функция обертка для генерации pdf по check_id,
     Добавляет функцию __generate_pdf__ в очередь, воркер её берет из очереди и выполняет.
     Принимает check.id, django_rq сериализует задачу в json и пишет в redis.
    """
    enqueue(__generate_pdf__, check_id=check_id)
