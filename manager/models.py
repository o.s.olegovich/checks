from django.db import models
from django.contrib.postgres.fields import JSONField


class Printer(models.Model):
    """
    Принтер (Printer). Каждый принтер печатает только свой тип чеков. Поле api_key принимает уникальные значения,
    по нему однозначно определяется принтер.
    """
    KITCHEN = 'kitchen'
    CLIENT = 'client'
    TYPES = [
        (KITCHEN, 'Кухня'),
        (CLIENT, 'Клиент'),
    ]

    name = models.CharField(max_length=255)
    api_key = models.CharField(max_length=255, primary_key=True)
    check_type = models.CharField(max_length=7, choices=TYPES)
    point_id = models.IntegerField()

    def __str__(self):
        return self.name


class Check(models.Model):
    """
    Чек (Check). Информация о заказе для каждого чека хранится в JSON, нет необходимости делать отдельные модели.
    | Поле       | Тип        | Значение               | Описание                     |
    | ---------- | ---------- | ---------------------- | ---------------------------- |
    | printer_id | ForeignKey |                        | принтер                      |
    | type       | CharField  | kitchen\|client        | тип чека                     |
    | order      | JSONField  |                        | информация о заказе          |
    | status     | CharField  | new\|rendered\|printed | статус чека                  |
    | pdf_file   | FileField  |                        | ссылка на созданный PDF-файл |
    """

    KITCHEN = 'kitchen'
    CLIENT = 'client'
    TYPES = [
        (KITCHEN, 'Кухня'),
        (CLIENT, 'Клиент'),
    ]

    NEW = 'new'
    RENDERED = 'rendered'
    PRINTED = 'printed'
    STATUSES = [
        (NEW, 'Новый'),
        (RENDERED, 'Готов к печати'),
        (PRINTED, 'Напечатанный'),
    ]

    id = models.AutoField(primary_key=True)
    printer_id = models.ForeignKey(Printer, on_delete=models.CASCADE)
    type = models.CharField(choices=TYPES, max_length=7)
    order = JSONField()
    status = models.CharField(choices=STATUSES, max_length=8, default=NEW)
    pdf_file = models.FileField(upload_to='media/pdf/', null=True, blank=True)
