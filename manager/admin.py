from django.contrib import admin
from .models import Printer, Check


@admin.register(Printer)
class PrinterAdmin(admin.ModelAdmin):
    """
    Настройки админки для Принтеров
    """
    list_display = ('api_key', 'name', 'check_type', 'point_id')
    list_display_links = ('api_key', 'name')


@admin.register(Check)
class CheckAdmin(admin.ModelAdmin):
    """
    Настройки админки для Чеков
    list_filter - вертикальный фильтр по полям
    """
    list_display = ('id', 'type', 'status', 'printer_id')
    list_display_links = ('id',)
    list_filter = ('type', 'status', 'printer_id')


admin.site.site_header = "Check's"
