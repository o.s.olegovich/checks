from django.test import TestCase

# Create your tests here.
from manager.models import Check, Printer
from django.urls import reverse
import json



class ManagerTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        printer = Printer(point_id=99999, api_key=99999999999, check_type=Printer.KITCHEN, name='Test')
        printer.save()

    def test_create_checks(self):
        order = {
            "id": 999999,
            "price": 780,
            "items": [
                {
                    "name": "Вкусная пицца",
                    "quantity": 2,
                    "unit_price": 250
                },
                {
                    "name": "Не менее вкусные роллы",
                    "quantity": 1,
                    "unit_price": 280
                }
            ],
            "address": "г. Уфа, ул. Ленина, д. 42",
            "client": {
                "name": "Иван",
                "phone": 9173332222
            },
            "point_id": 99999
        }
        resp = self.client.post('/create_checks/', json.dumps(order), content_type="application/json")
        self.assertEqual(resp.status_code, 200)
        self.assertTrue(Check.objects.filter(order__id=999999))

    def test_new_checks(self):
        resp = self.client.get('/new_checks/?api_key=99999999999')
        self.assertEqual(resp.status_code, 200)

    def test_check(self):
        resp = self.client.get('/new_checks/?api_key=99999999999&check_id=999999')
        if resp.status_code != 200:
            print("Запустите воркер python3 manage.py rqworker default")
            self.assertFalse()

        self.assertEqual(resp.status_code, 200)


