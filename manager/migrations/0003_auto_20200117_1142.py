# Generated by Django 3.0.2 on 2020-01-17 11:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('manager', '0002_auto_20200116_2111'),
    ]

    operations = [
        migrations.AlterField(
            model_name='check',
            name='id',
            field=models.IntegerField(primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='check',
            name='pdf_file',
            field=models.FileField(blank=True, null=True, upload_to='media/pdf/'),
        ),
    ]
