from django.http import HttpResponse, JsonResponse
import json
from .models import Check, Printer
from django.views.decorators.csrf import csrf_exempt
from .tasks import generate_pdf
from django.views.decorators.http import require_http_methods


@require_http_methods(["GET"])
def check(request):
    """
    Запрос на получение pdf файла чека
    """
    api_key = request.GET.get('api_key')
    if api_key is None:
        return JsonResponse({"error": "Ошибка авторизации"}, status=401)

    # Есть ли клиент такой принтер
    printer = Printer.objects.filter(api_key=api_key)
    if printer.exists() is False:
        return JsonResponse({"error": "Ошибка авторизации"}, status=401)

    check_id = request.GET.get('check_id')

    # Есть ли такой чек
    if check_id is None or Check.objects.filter(printer_id__api_key=api_key, id=check_id).exists() is False:
        return JsonResponse({"error": "Данного чека не существует"}, status=400)

    if Check.objects.filter(id=check_id, status=Check.RENDERED).exists() is False:
        return JsonResponse({"error": "Для данного чека не сгенерирован PDF-файл"}, status=400)

    check = Check.objects.get(printer_id__api_key=api_key, id=check_id)
    file = open(check.pdf_file.name, 'rb')
    response = HttpResponse(content=file)
    # Указываем MIME, что-бы клиент нас правильно понял
    response['Content-Type'] = 'application/pdf'
    check.status = Check.PRINTED
    check.save()
    return response


@require_http_methods(["GET"])
def new_checks(request):
    """
    Отдаем чеки по принтеру (api_key)
    """
    api_key = request.GET.get('api_key')
    if Printer.objects.filter(api_key=api_key).exists():
        checks = Check.objects.filter(printer_id__api_key=api_key)
        return JsonResponse({'checks': list(checks.values('id'))})

    return JsonResponse({"error": "Ошибка авторизации"}, status=401)


@require_http_methods(["POST"])
@csrf_exempt
def create_checks(request):
    """

    Создаем чеки в базе и ставим задачи на генерацию pdf
    """
    # Десириализируем order
    json_order = json.loads(request.body)
    # Проверяем есть ли такой id в базе
    if Check.objects.filter(order__id=json_order['id']).exists():
        return JsonResponse({"error": "Для данного заказа уже созданы чеки"}, status=400)
        # Проверяем есть ли принтер у точки
    if Printer.objects.filter(point_id=json_order['point_id']).exists() is False:
        return JsonResponse({"error": "Для данной точки не настроено ни одного принтера"}, status=400)
    # Создаем чек для всех принетров
    for printer in Printer.objects.filter(point_id=json_order['point_id']):
        # Создаем чек в базе
        check = Check(printer_id=printer, type=printer.check_type, order=json_order)
        check.save()
        # Ставим задачу на генирацию чека
        generate_pdf(check_id=check.id)

    return JsonResponse({"ok": "Чеки успешно созданы"})
